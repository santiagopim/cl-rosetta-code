;;;; Rosetta Code
;;;; http://www.rosettacode.org

;; http://www.rosettacode.org/wiki/Loop_over_multiple_arrays_simultaneously
(do ((x '("a" "b" "c") (rest x))	;
     (y '("A" "B" "C" "D") (rest y))	;
     (z '(1 2 3 4 6) (rest z)))	; Initialize lists and set to rest on every loop
    ((or (null x) (null y) (null z)))	; Break condition 
  (format t "~a~a~a~%" (first x) (first y) (first z))) ; On every loop print first elements

;; http://www.rosettacode.org/wiki/Loops/Break
(do ((a (random 20) (random 20)))	; Initialize to rand and set new rand on every loop
    ((= a 10) (write a))		; Break condition and last step
  (format t "~a~3T~a~%" a (random 20)))	; On every loop print formated `a' and rand `b'

;; http://www.rosettacode.org/wiki/Loops/Do-while
(do* ((a 0)		     ; Initialize to 0
      (b (incf a) (incf b))) ; Set first increment and increment on every loop
     ((zerop (mod b 6)) (print b)) ; Break condition and print last value `6' (right?)
  (print b))			   ; On every loop print value

;; http://www.rosettacode.org/wiki/Loops/Downward_for
(do ((n 10 (decf n)))			; Initialize to 0 and downward in every loop
    ((< n 0))				; Break condition when negative
  (print n))				; On every loop print value

;; http://www.rosettacode.org/wiki/Loops/For_with_a_specified_step
(do ((n 0 (incf n (+ (random 3) 2))))	; Initialize to 0 and set random step-value 2, 3 or 4
    ((> n 20))				; Break condition
  (print n))				; On every loop print value

;; http://www.rosettacode.org/wiki/Loops/Foreach
(let ((the-list '(1 7 "foo" 1 4)))	; Set the-list as the list
  (do ((i the-list (rest i)))		; Initialize to the-list and set to rest on every loop
      ((null i))			; Break condition
    (print (first i))))	    ; On every loop print list's first element

;; http://www.rosettacode.org/wiki/Loops/Increment_loop_index_within_loop_body
(defun primep (n)			; https://stackoverflow.com/questions/15817350/
  (cond ((= 2 n) t)			; Hard-code "2 is a prime"
        ((= 3 n) t)			; Hard-code "3 is a prime"
        ((evenp n) nil) ; If we're looking at an even now, it's not a prime
        (t ; If it is divisible by an odd number below its square root, it's not prime
	 (do* ((i 3 (incf i 2)))	; Initialize to 3 and increment by 2 on every loop
	      ((or (> i (isqrt n))	; Break condition index exceeds its square root
		   (zerop (mod n i)))	; Break condition it is divisible
	       (not (zerop (mod n i)))))))) ; Returns not divisible, aka prime

(do ((i 42)		    ; Initialize index to 42
     (c 0))		    ; Initialize count of primes to 0
    ((= c 42))		    ; Break condition when there are 42 primes
  (incf i)		    ; Increments index by unity
  (if (primep i) (progn (incf c)	  ; If prime increment count of primes
			(format t "~&~5<~d~;->~>~20<~:d~>" c i) ; Display count of primes found and the prime
			(incf i (decf i))))) ; Increment index to previous index plus the prime

;; http://www.rosettacode.org/wiki/Loops/Infinite
(do ()					; Not initialization
    (nil)				; Not break condition
  (print "SPAM"))			; On every loop as requested

;; http://www.rosettacode.org/wiki/Loops/N_plus_one_half
(do ((i 1 (incf i)))			; Initialize to 1 and increment on every loop
    ((> i 10))				; Break condition
  (princ i)				; Print the iteration number
  (when (= i 10) (go end))		; Use the implicit tagbody and go to end tag when reach the last iteration
  (princ ", ")				; Printing the comma is jumped by the go statement
 end)					; The tag

(do	; Not exactly separate statements for the number and the comma
 ((i 1 (incf i)))	 ; Initialize to 1 and increment on every loop
 ((> i 9) (princ i)) ; Break condition when iteration is the last number, print it
  (princ i)	     ; Print number statement
  (princ ", "))	     ; Print comma statement

;; https://rosettacode.org/wiki/Loops/with_multiple_ranges
;; TODO: Set a range constructor or macro to iterate ranges as in Python

(let ((prod 1)				; Initialize aggregator
      (sum 0)
      (x 5)				; Initialize variables
      (y -5)
      (z -2)
      (one 1)
      (three 3)
      (seven 7))

  (flet ((loop-body (j)			; Set the loop function
	    (incf sum (abs j))
	    (if (and (< (abs prod) (expt 2 27))
		     (/= j 0))
		(setf prod (* prod j)))))

    (do ((i (- three) (incf i three)))	; Just a serie of individual loops
	((> i (expt 3 3)))
      (loop-body i))
    (do ((i (- seven) (incf i x)))
	((> i seven))
      (loop-body i))
    (do ((i 555 (incf i -1)))
	((< i (- 550 y)))
      (loop-body i))
    (do ((i 22 (incf i (- three))))
	((< i -28))
      (loop-body i))
    (do ((i 1927 (incf i)))
	((> i 1939))
      (loop-body i))
    (do ((i x (incf i z)))
	((< i y))
      (loop-body i))
    (do ((i (expt 11 x) (incf i)))
	((> i (+ (expt 11 x) one)))
      (loop-body i)))

  (format t "~&sum  = ~14<~:d~>" sum)
  (format t "~&prod = ~14<~:d~>" prod))

;; With loop ranges and increments as list to dolist as in Julia
(let ((prod 1)
      (sum 0)
      (x 5)
      (y -5)
      (z -2)
      (one 1)
      (three 3)
      (seven 7))

  (flet ((loop-body (j)			; Set the loop function
	   (incf sum (abs j))
	   (if (and (< (abs prod) (expt 2 27))
		    (/= j 0))
	       (setf prod (* prod j)))))

    (dolist (lst `((,(- three) ,(expt 3 3) ,three)
		   (,(- seven) ,seven ,x)
		   (555 ,(- 550 y) -1)
		   (22 -28 ,(- three))
		   (1927 1939 1)
		   (,x ,y ,z)
		   (,(expt 11 x) ,(+ (expt 11 x) one) 1)))
      (do ((i (car lst) (incf i (caddr lst))))
	  ((if (plusp (caddr lst))
	       (> i (cadr lst))
	       (< i (cadr lst))))
	(loop-body i))))

  (format t "~&sum  = ~14<~:d~>" sum)
  (format t "~&prod = ~14<~:d~>" prod))

;; http://www.rosettacode.org/wiki/Loops/Wrong_ranges
;; TODO: Improve with function or macro
(dolist (lst '((-2  2  1 "Normal")	; Iterate the parameters list `start' `stop' `increment' and `comment'
	       (-2  2  0 "Zero increment")
	       (-2  2 -1 "Increments away from stop value")
	       (-2  2 10 "First increment is beyond stop value")
	       ( 2 -2  1 "Start more than stop: positive increment")
	       ( 2  2  1 "Start equal stop: positive increment")
	       ( 2  2 -1 "Start equal stop: negative increment")
	       ( 2  2  0 "Start equal stop: zero increment")
	       ( 0  0  0 "Start equal stop equal zero: zero increment")))
  (do ((i (car lst) (incf i (caddr lst))) ; Initialize `start' and set `increment'
       (result ())			  ; Initialize the result list
       (loop-max 0 (incf loop-max)))	  ; Initialize a loop limit
      ((or (> i (cadr lst))		  ; Break condition to `stop'
	   (> loop-max 10))		  ; Break condition to loop limit
       (format t "~&~44a: ~{~3d ~}"	  ; Finally print
	       (cadddr lst)		  ; The `comment'
	       (reverse result)))	  ; The in(de)creased numbers into result
    (push i result)))			  ; Add the number to result
